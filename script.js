// Create student one

let studentOneName = "John";
let studentOneEmail = "john@mail.com";
let studentOneGrades = [89, 84, 78, 88];

// Create student two

let studentTwoName = "Joe";
let studentTwoEmail = "joe@mail.com";
let studentTwoGrades = [78, 82, 79, 85];

// Create student three

let studentThreeName = "Jane";
let studentThreeEmail = "jane@mail.com";
let studentThreeGrades = [87, 89, 91, 93];

// Create student four

let studentFourName = "Jessie";
let studentFourEmail = "jessie@mail.com";
let studentFourGrades = [91, 89, 92, 93];

function login(email) {
  console.log(`${email} has logged in`);
}

login(studentOneEmail);

function logout(email) {
  console.log(`${email} has logged out`);
}

function listGrades(grades) {
  grades.forEach(grade => {
    console.log(grade);
  })
}

// spaghetti code - code that is so poorly organized

// Use an object literal: {}

// ENCAPSULATION - the organization of information (properties) and behavior (as methods) to belong to the object that encapsulates them (the scope of encapsulation is denoted by the object literals)

let studentOne = {
  name: "John",
  email: "john@mail.com",
  grades: [89, 84, 78, 88],

  // add the functionalities available to a student as object methods
    // the keyword "this" refers to the object encapsulating the method where "this" is called
  
  login() {
    console.log(`${this.email} has logged in`);
  },

  logout() {
    console.log(`${this.email} has logged out`);
  },

  listGrades() {
    console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
  },

  // Mini Exercise

  // Create a function that will GET/compute the quarterly average of studentOne's grades.

  getAve() {
    let total = 0;
    for (let i = 0; i < this.grades.length; i++) {
    total += this.grades[i];
    }
    return total / this.grades.length;
  },

  // Teacher Miah's version
  
  computeAve() {
   let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);
    
    return sum / 4; 
  },

  // Mini Exercise 2

    // Create a function that will return true if the average grade is >= 85, false if not

  willPass() {
    // hint: you can call methods inside an object
    let sum = 0;
    this.grades.forEach(grade => sum = sum + grade);

    let ave = sum / 4;
    
    if (ave >= 85) {
      console.log(true);
    } else {
      console.log(false);
    }
  },

  /* Other version:
  if(this.computeAve() >= 85) {
  return true
  } else {
  return false
  }
  */
  // better version

  // you can use: return this.computeAve() >= 85;

  willPassTwo() {
    return this.computeAve() >= 85 ? true : false

    // turnary operator; condition ? value if condition is true : value if condition is false
  },

  // Mini Exercise 3
  // Create a function called willPassWithHonors() that returns true if the student has passed AND their average grade is >= 90. The function return false if either one is not met.
  willPassWithHonors() {
    return this.computeAve() >= 90 && this.willPass() ? true : false;
  },

  //Miah's version
  willPassWithHonorsTwo() {
    return (this.willPass() && this.computeAve() >= 90);
  }
};

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's name is ${studentOne.email}`);
console.log(`student one's name is ${studentOne.grades}`);
studentOne.login();
studentOne.listGrades();
studentOne.getAve();
studentOne.willPass();
studentOne.willPassWithHonorsTwo();
studentOne.logout();

/*

Activity

Quiz:

1. What is the term given to unorganized code that's very hard to work with?

Spaghetti code

2. How are object literals written in JS?

It's written in curly braces {}.

3. What do you call the concept of organizing information and functionality to belong to an object?

Object-oriented programming (OOP)

4. If studentOne has a method named enroll(), how would you invoke it?

You use the method.

5. True or False: Objects can have objects as properties.


6. What is the syntax in creating key-value pairs?
7. True or False: A method can have no parameters and still work.
8. True or False: Arrays can have objects as elements.
9. True or False: Arrays are objects.
10. True or False: Objects can have arrays as properties.

Function Coding:

1. Translate the other students from our boilerplate code into their own respective objects.

2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

3. Define a method for all student objects named willPass() that returns a Boolean value
indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

4. Define a method for all student objects named willPassWithHonors() that returns true
if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

5. Create an object named classOf1A with a property named students
which is an array containing all 4 student objects in it.

6. Create a method for the object classOf1A named countHonorStudents()
that will return the number of honor students.

7. Create a method for the object classOf1A named honorsPercentage()
that will return the % of honor students from the batch's total number of students.

8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.

9. Create a method for the object classOf1A named
sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
*/